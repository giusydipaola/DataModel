//
//  ContentView.swift
//  DataModel
//
//  Created by Giusy Di Paola on 08/11/21.
//

import SwiftUI

struct ContentView: View {
    var pizza : [Pizza] = PizzaData
  
    var body: some View {
        NavigationView {
        List {
            ForEach(pizza) {  item in
                NavigationLink(destination:
                    SecondPage(pizza: item)
                ){
                Rowdetail(pizza: item)
                }
            }
         }//: end list
        .navigationTitle("PIZZE")
        }//: end Navigationview
    }//: end body
}//:end contentView

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
