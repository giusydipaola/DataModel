//
//  DataModelApp.swift
//  DataModel
//
//  Created by Giusy Di Paola on 08/11/21.
//

import SwiftUI

@main
struct DataModelApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
