//
//  PizzaData.swift
//  DataModel
//
//  Created by Giusy Di Paola on 08/11/21.
//

import SwiftUI


let PizzaData : [Pizza] =
[
    Pizza(titolo: "Margherita", descrizione: "Pizza margherita"),
    Pizza(titolo: "Napoletana", descrizione: "Pizza napoletana"),
    Pizza(titolo: "Capricciosa", descrizione: "Pizza capricciosa"),
    Pizza(titolo: "Porcina", descrizione: "Pizza Porcina")
]
    

