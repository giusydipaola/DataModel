//
//  Rowdetail.swift
//  DataModel
//
//  Created by Giusy Di Paola on 08/11/21.
//

import SwiftUI

struct Rowdetail: View {
    
    
//: MARK PROPERTIES
    var pizza : Pizza
    
    var body: some View {
        
        VStack() {
        Text(pizza.titolo)
        Text(pizza.descrizione)
            
       }
    }
}

struct Rowdetail_Previews: PreviewProvider {
    static var previews: some View {
        Rowdetail(pizza: PizzaData[0])
    }
}
