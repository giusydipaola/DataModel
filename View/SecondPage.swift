//
//  SecondPage.swift
//  DataModel
//
//  Created by Giusy Di Paola on 08/11/21.
//

import SwiftUI

struct SecondPage: View {
    
    var pizza : Pizza
    var body: some View {
    
NavigationView{
        VStack() {
            Text(pizza.titolo)
                .font(.title)
                .fontWeight(.black)
            Text(pizza.descrizione)
                .fontWeight(.medium)
                .foregroundColor(Color.red)
          }.navigationTitle("Pizza View")
        }
    
    }
    
}

struct SecondPage_Previews: PreviewProvider {
    static var previews: some View {
        SecondPage(pizza: PizzaData[0])
    }
}
