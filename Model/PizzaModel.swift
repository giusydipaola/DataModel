//
//  PizzaModel.swift
//  DataModel
//
//  Created by Giusy Di Paola on 08/11/21.
//

import SwiftUI

//test 
struct Pizza : Identifiable
{
    var id = UUID()
    var titolo : String
    var descrizione : String
}
